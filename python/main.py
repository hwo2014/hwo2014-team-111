import json
import socket
import sys


class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        self.throttle(0.65)

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def change_lane(self, data): #data: "Left" or "Right"
        self.msg("switchLane", data)
            

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        distance = 5 #what distance in the future we look #best 5
        
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type == 'gameInit':
                game_map = data
                pieces = game_map['race']['track']['pieces']
                size_race = len(pieces)
                nb_laps = game_map['race']['raceSession']['laps']
                print 'pieces', pieces
                pieces = pieces * nb_laps
            
            elif msg_type == 'carPositions':
                cpc = data[0]['piecePosition']['pieceIndex'] # current_piece_position
                total = sum([el['angle'] for el in pieces[cpc:cpc+distance] if 'angle' in el])
                #print 'total', total

                if 'switch' in pieces[(cpc+1)%size_race]:
                    if total > 0:
                        self.change_lane("Right")
                    elif total < 0:
                        self.change_lane("Left")
                    else:
                        #print 'switch, total=0'
                        self.throttle(0.67) #max 0.67, 0.69 crashed
                else:
                    if total == 0:
                        self.throttle(0.72) #max 0.72, 0.73 crashed
                    elif abs(total) > 45:
                        self.throttle(0.63) #max 0.63, 0.64 crashed
                    else:
                        self.throttle(0.66) #max 0.66, 0.67 crashed

            elif msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
